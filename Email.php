<?php
declare(strict_types=1);

namespace email;

require 'vendor/autoload.php';
require __DIR__ . '/mailjet/src/Mailjet/Client.php';

use Swift_Events_EventDispatcher;
use Swift_Events_EventListener;
use Swift_Mime_SimpleMessage;
use Swift_Transport;

class Email implements Swift_Transport
{
    private static $apiKey = '198dfc8b0b89815a514e7f25e6976009';
    private static $apiSecret = 'e63eab19aa477a80b238229bd0f2c328';
    private $started = false;
    private $eventDispatcher;
    private $client; // klient serwera SMTP utworzony podczas uruchamiania mechanizmu Transportera i użyty w metodzie send

    /**
     * Email constructor.
     * @param Swift_Events_EventDispatcher $dispatcher
     */
    public function __construct(Swift_Events_EventDispatcher $dispatcher)
    {
        $this->eventDispatcher = $dispatcher;
    }

    /**
     * Test if this Transport mechanism has started.
     *
     * @return bool
     */
    public function isStarted()
    {
        return $this->started;
    }

    /**
     * Start this Transport mechanism.
     */
    public function start()
    {
        if (!$this->started) {
            if ($evt = $this->eventDispatcher->createTransportChangeEvent($this)) {
                $this->eventDispatcher->dispatchEvent($evt, 'beforeTransportStarted');
                if ($evt->bubbleCancelled()) {
                    return;
                }
            }
            if ($evt) {
                $this->eventDispatcher->dispatchEvent($evt, 'transportStarted');
            }
            $this->started = true;
            $this->client = new Client(self::$apiKey, self::$apiSecret, true,
                ['url' => "www.mailjet.com", 'version' => 'v3', 'call' => false]);
        }
    }

    /**
     * Stop this Transport mechanism.
     */
    public function stop()
    {
        if ($this->started) {
            if ($evt = $this->eventDispatcher->createTransportChangeEvent($this)) {
                $this->eventDispatcher->dispatchEvent($evt, 'beforeTransportStopped');
                if ($evt->bubbleCancelled()) {
                    return;
                }
            }
        }
        $this->started = false;
    }

    /**
     * Check if this Transport mechanism is alive.
     *
     * If a Transport mechanism session is no longer functional, the method
     * returns FALSE. It is the responsibility of the developer to handle this
     * case and restart the Transport mechanism manually.
     *
     * @example
     *
     *   if (!$transport->ping()) {
     *      $transport->stop();
     *      $transport->start();
     *   }
     *
     * The Transport mechanism will be started, if it is not already.
     *
     * It is undefined if the Transport mechanism attempts to restart as long as
     * the return value reflects whether the mechanism is now functional.
     *
     * @return bool TRUE if the transport is alive
     */
    public function ping()
    {
        if (!$this->isStarted()) {
            $this->start();
        }
        return true;
    }

    /**
     * Send the given Message.
     *
     * Recipient/sender data will be retrieved from the Message API.
     * The return value is the number of recipients who were accepted for delivery.
     *
     * @param Swift_Mime_SimpleMessage $message
     * @param string[] $failedRecipients An array of failures by-reference
     *
     * @return int
     */
    public function send(Swift_Mime_SimpleMessage $message, &$failedRecipients = null)
    {
        $body = [
            'From' => $this->convertAddresses($message->getSender()), //nie wiemy czy ktoś podał parametr name
            'Subject' => $message->getSubject(),
            'Text-part' => $message->getBody(),
            'Html-part' => "test",
            'Recipients' => [
                [
                    'Email' => $this->convertAddresses($message->getTo())
                ]
            ]
        ];
        $response = $this->client->post(['send', ''/*, 'v3.1'*/], ['body' => $body]);
        $response->success() && var_dump($response->getData());
    }

    private function convertAddresses($addresses)
    {
        $address = array();
        foreach ($addresses as $email => $name) {
            if (!is_null($name)) {
                array_push($address, ['Email' => $email, 'Name' => $name]);
            } else {
                array_push($address, ['Email' => $email]);
            }
        }
        return $address;
    }

    /**
     * Register a plugin in the Transport.
     *
     * @param Swift_Events_EventListener $plugin
     */
    public function registerPlugin(Swift_Events_EventListener $plugin)
    {
        $this->eventDispatcher->bindEventListener($plugin);
    }

    /**
     * Destructor.
     */
    public function __destruct()
    {
        try {
            $this->stop();
        } catch (\Throwable | \Error | \Exception $e) {
            echo $e->getMessage();
        }
    }
}
