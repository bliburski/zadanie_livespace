<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class EmailTest extends TestCase
{
    protected $dispatcher;
    protected $headers;
    protected $encoder;
    protected $cache;
    protected $idGenerator;
    protected function setUp() {
        $this->dispatcher = $this->createMock('\Swift_Events_EventDispatcher');
        $this->headers = $this->createMock('\Swift_Mime_SimpleHeaderSet');
        $this->encoder = $this->createMock('\Swift_Mime_ContentEncoder');
        $this->cache = $this->createMock('\Swift_KeyCache');
        $this->idGenerator = $this->createMock('\Swift_IdGenerator');
    }

    public function testSendTextEmail() {
        $message = new Swift_Mime_SimpleMessage($this->headers, $this->encoder, $this->cache, $this->idGenerator);
        $message->setSender('test@t.pl');
        $message->setSubject('Test');
        $message->setBody('test', 'text/plain');
        $message->setTo('nfstyle03@gmail.pl');

        $email = new \email\Email($this->dispatcher);
        $email->send($message);
        $this->assertEquals(200, http_response_code());
    }
}
